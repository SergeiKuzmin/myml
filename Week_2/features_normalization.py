import numpy as np
import pandas
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

data_train = pandas.read_csv('perceptron-train.csv')
y_train = np.array(data_train['Answer'])
x_train = np.array([data_train['Feature_1'],
                    data_train['Feature_2']]).T
clf = Perceptron(random_state=241)
clf.fit(x_train, y_train)

data_test = pandas.read_csv('perceptron-test.csv')
y_test = np.array(data_test['Answer'])
x_test = np.array([data_test['Feature_1'],
                   data_test['Feature_2']]).T
y_pred = clf.predict(x_test)
accuracy_non_scale = accuracy_score(y_test, y_pred)
print(accuracy_non_scale)

clf_scale = Perceptron()
scaler = StandardScaler()
x_train_scale = scaler.fit_transform(x_train)
print(x_train_scale.T[0].mean(), x_train_scale.T[0].std())
clf_scale.fit(x_train_scale, y_train)
x_test_scale = scaler.transform(x_test)
print(x_test_scale.T[0].mean(), x_test_scale.T[0].std())
y_pred = clf_scale.predict(x_test_scale)
accuracy_scale = accuracy_score(y_test, y_pred)
print(accuracy_scale)
print('Difference for score = ', accuracy_scale - accuracy_non_scale)

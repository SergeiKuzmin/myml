import pandas
import numpy as np
from sklearn.preprocessing import scale
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_boston
import sklearn

data = load_boston()
non_scale_x = data['data']
y = data['target']

x = scale(non_scale_x)

n_splits = 5
gen_split = KFold(n_splits=5, shuffle=True, random_state=42)

score_list = []
array_p = np.linspace(1, 10, 200)
print(array_p.shape)
for p in array_p:
    reg = KNeighborsRegressor(n_neighbors=5, weights='distance', p=p, metric='minkowski')
    score = cross_val_score(reg, x, y,  scoring='neg_mean_squared_error', cv=gen_split).mean()
    score_list.append((p, score))

p_max = 1
score_max = np.array(score_list).T[1].max()
for p, score in score_list:
    if score == score_max:
        p_max = p
print('Task 1: ', p_max)

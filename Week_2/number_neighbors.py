import pandas
import numpy as np
from sklearn.preprocessing import scale
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score

data = pandas.read_csv('data_wine.txt')

classes = np.array(data['Class'])

non_scale_matrix_features = np.array([data['Alcohol'],
                                      data['Malic acid'],
                                      data['Ash'],
                                      data['Alcalinity of ash'],
                                      data['Magnesium'],
                                      data['Total phenols'],
                                      data['Flavanoids'],
                                      data['Nonflavanoid phenols'],
                                      data['Proanthocyanins'],
                                      data['Color intensity'],
                                      data['Hue'],
                                      data['OD280/OD315 of diluted wines'],
                                      data['Proline']]).T

matrix_features = scale(non_scale_matrix_features)

n_splits = 5
gen_split = KFold(n_splits=5, shuffle=True, random_state=42)

# Task 1, 2
score_list = []
for k in range(1, 51, 1):
    clf = KNeighborsClassifier(n_neighbors=k)
    score = cross_val_score(clf, non_scale_matrix_features, classes, cv=gen_split).mean()
    score_list.append((k, score))

k_max = 1
score_max = np.array(score_list).T[1].max()
for k, score in score_list:
    if score == score_max:
        k_max = k

print('Task 1: ', k_max)
print('Task 2: ', score_max)

# Task 3, 4
score_list = []
for k in range(1, 51, 1):
    clf = KNeighborsClassifier(n_neighbors=k)
    score = cross_val_score(clf, matrix_features, classes, cv=gen_split).mean()
    score_list.append((k, score))

k_max = 1
score_max = np.array(score_list).T[1].max()
for k, score in score_list:
    if score == score_max:
        k_max = k

print('Task 3: ', k_max)
print('Task 4: ', score_max)

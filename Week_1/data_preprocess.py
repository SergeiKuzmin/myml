import pandas
import numpy as np

data = pandas.read_csv('titanic.csv', index_col='PassengerId')

# 1
print('Task 1:')
male_number = np.array([int(data['Sex'][i] == 'male') for i in range(1, len(data['Sex']) + 1, 1)]).sum()
female_number = np.array([int(data['Sex'][i] == 'female') for i in range(1, len(data['Sex']) + 1, 1)]).sum()
print('Male number = ', male_number)
print('Female number = ', female_number)

print()

# 2
print('Task 2:')
survived_number = data['Survived'].sum()
common_number = len(data['Survived'])
percent_survived = (survived_number / common_number) * 100
print('Percent survived = ', percent_survived)

print()

# 3
print('Task 3:')
pclass_number = np.array([int(data['Pclass'][i] == 1) for i in range(1, len(data['Pclass']) + 1, 1)]).sum()
percent_pclass = (pclass_number / common_number) * 100
print('Percent Premier class = ', percent_pclass)

print()

# 4
print('Task 4:')
age_mean = data['Age'].mean()
age_median = data['Age'].median()
print('Age mean = ', age_mean, '; Age median = ', age_median)

print()

# 5
print('Task 5:')
data_frame = pandas.DataFrame(data, columns=['SibSp', 'Parch'])
corr_pearson = data_frame.corr(method='pearson')
print('Corr Pearson = ', corr_pearson)

print()

# 6
print('Task 6:')
list_name = []
for i in range(1, len(data['Name']) + 1, 1):
    if (data['Sex'][i] == 'female') and \
            (data['Name'][i].split()[1] == 'Mrs.' or data['Name'][i].split()[1] == 'Miss.'):
        name = (data['Name'][i].split())[2]
        if name[0] == '(':
            name = name[1:]
        if name[-1] == ')':
            name = name[:-1]
        list_name.append(name)
print(sorted(list_name))

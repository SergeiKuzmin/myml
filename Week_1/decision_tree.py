import numpy as np
import pandas
from sklearn.tree import DecisionTreeClassifier

data = pandas.read_csv('titanic.csv', index_col='PassengerId')
data_training = []
data_outputs = []
for i in range(1, len(data['Pclass']) + 1, 1):
    if ((not np.isnan(data['Pclass'][i])) and (not np.isnan(data['Fare'][i])) and (not np.isnan(data['Age'][i])) and
            (not np.isnan(data['Survived'][i]))):
        sex = 1
        if data['Sex'][i] == 'male':
            sex = 1
        else:
            sex = -1
        data_training.append([data['Pclass'][i], data['Fare'][i], data['Age'][i], sex])
        data_outputs.append(data['Survived'][i])

X = np.array(data_training)
Y = np.array(data_outputs)
clf = DecisionTreeClassifier(random_state=241)
clf.fit(X, Y)
importances = clf.feature_importances_
print(importances)

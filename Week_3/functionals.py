import numpy as np


def log_reg(x, y, weights, c):
    l = x.shape[0]
    func = 0
    for i in range(0, l, 1):
        func += np.log(1 + np.exp(-y[i] * np.dot(weights, x[i])))
    func /= l
    func += (1 / 2) * c * (np.abs(weights) ** 2).sum()
    return func


def grad_log_reg(x, y, weights, c):
    l = x.shape[0]
    grad_func = np.zeros_like(weights)
    for i in range(0, l, 1):
        grad_func -= y[i] * x[i] * (1 - (1 / (1 + np.exp(-y[i] * np.dot(weights, x[i])))))
    grad_func /= l
    grad_func += c * weights
    return grad_func


def a(x, weights):
    l = x.shape[0]
    proba = np.zeros(l)
    for i in range(0, l, 1):
        proba[i] = 1 / (1 + np.exp(-np.dot(weights, x[i])))
    return proba

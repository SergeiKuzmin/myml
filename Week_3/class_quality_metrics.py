import numpy as np
import pandas
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve

data = pandas.read_csv('classification.csv')

data_true = np.array(data['true'])
data_pred = np.array(data['pred'])

# Task 1
print('Task 1')
TP = 0
FP = 0
FN = 0
TN = 0
for i in range(len(data_true)):
    if (data_pred[i] == 1) and (data_true[i] == 1):
        TP += 1
    if (data_pred[i] == 1) and (data_true[i] == 0):
        FP += 1
    if (data_pred[i] == 0) and (data_true[i] == 1):
        FN += 1
    if (data_pred[i] == 0) and (data_true[i] == 0):
        TN += 1

print('TP = ', TP, 'FP = ', FP, 'FN = ', FN, 'TN = ', TN)

# Task 2
print('Task 2')
prec = TP / (TP + FP)
rec = TP / (TP + FN)
print('Accuracy = ', accuracy_score(data_true, data_pred), 'Precision = ', precision_score(data_true, data_pred),
      'Recall = ', recall_score(data_true, data_pred), 'F-measure = ', f1_score(data_true, data_pred))
print('Accuracy = ', (TP + TN) / (TP + TN + FN + FP), 'Precision = ', TP / (TP + FP),
      'Recall = ', TP / (TP + FN), 'F-measure = ', 2 * prec * rec / (prec + rec))

data = pandas.read_csv('scores.csv')

data_true = np.array(data['true'])
data_logreg = np.array(data['score_logreg'])
data_svm = np.array(data['score_svm'])
data_knn = np.array(data['score_knn'])
data_tree = np.array(data['score_tree'])

# Task 3
print('Task 3')
print('logreg = ', roc_auc_score(data_true, data_logreg), 'svm = ', roc_auc_score(data_true, data_svm),
      'knn = ', roc_auc_score(data_true, data_knn), 'tree = ', roc_auc_score(data_true, data_tree))

# Task 4
print('Task 4')

prec, rec, threshold = precision_recall_curve(data_true, data_logreg)
max_prec = 0
for i in range(len(prec)):
    if rec[i] >= 0.7:
        max_prec = prec[i]
print('max_prec for logreg = ', max_prec)

prec, rec, threshold = precision_recall_curve(data_true, data_svm)
max_prec = 0
for i in range(len(prec)):
    if rec[i] >= 0.7:
        max_prec = prec[i]
print('max_prec for svm = ', max_prec)

prec, rec, threshold = precision_recall_curve(data_true, data_knn)
max_prec = 0
for i in range(len(prec)):
    if rec[i] >= 0.7:
        max_prec = prec[i]
print('max_prec for knn = ', max_prec)

prec, rec, threshold = precision_recall_curve(data_true, data_tree)
max_prec = 0
for i in range(len(prec)):
    if rec[i] >= 0.7:
        max_prec = prec[i]
print('max_prec for tree = ', max_prec)

import numpy as np
import pandas
from sklearn.metrics import roc_auc_score
from Week_3.functionals import log_reg, grad_log_reg, a

data_train = pandas.read_csv('data-logistic.csv')
y_train = np.array(data_train['Answer'])
x_train = np.array([data_train['Feature_1'],
                    data_train['Feature_2']]).T

weights = np.zeros(2)

print(weights)
print(x_train.shape)
print(y_train.shape)

k = 0.1
C = 10
for i in range(100):
    print('Number of iteration: ', i, ' functional: ', log_reg(x_train, y_train, weights, C))
    weights -= k * grad_log_reg(x_train, y_train, weights, C)
    score = roc_auc_score(y_train, a(x_train, weights))
    print('Score: ', score)

score = roc_auc_score(y_train, a(x_train, weights))
print(score)

import numpy as np
import pandas
from sklearn.svm import SVC

data_train = pandas.read_csv('svm-data.csv')

y_train = np.array(data_train['Answer'])
x_train = np.array([data_train['Feature_1'],
                    data_train['Feature_2']]).T
clf = SVC(C=100000, kernel='linear', random_state=241)
clf.fit(x_train, y_train)
print(clf.support_)

import numpy as np
from scipy.sparse import csr_matrix
from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score

newsgroups = datasets.fetch_20newsgroups(subset='all', categories=['alt.atheism', 'sci.space'])

vectorizer = TfidfVectorizer()
x_train = vectorizer.fit_transform(newsgroups.data)
y_train = newsgroups.target

print(x_train.shape)
print(y_train.shape)

feature_mapping = vectorizer.get_feature_names()

grid = np.power(10.0, np.arange(-5, 6))
cv = KFold(n_splits=5, shuffle=True, random_state=241)

score_list = []
for C in grid:
    clf = SVC(C=C, kernel='linear', random_state=241)
    score = cross_val_score(clf, x_train, y_train,  scoring='accuracy', cv=cv).mean()
    score_list.append((C, score))
print(score_list)

clf = SVC(C=100000, kernel='linear', random_state=241)
clf.fit(x_train, y_train)
print((csr_matrix(clf.coef_).toarray()).shape)
weights = list(np.abs((csr_matrix(clf.coef_).toarray()).reshape(-1)))
sorted_weights = sorted(weights)
max_weights = sorted_weights[-10:]
list_words = []
for i, word in enumerate(feature_mapping):
    for weight in max_weights:
        if weight == weights[i]:
            list_words.append(word)
print(sorted(list_words))

from numba import jit
import numpy as np
import time


@jit
def my_sum_numba(n):
    s = 0.0
    for m in range(n):
        s += np.sqrt(m)
    return s


def my_sum(n):
    s = 0.0
    for m in range(n):
        s += np.sqrt(m)
    return s


my_sum(100)
t1 = time.time()
S = my_sum(10000000)
print(S)
print(time.time() - t1)

my_sum_numba(100)
t1 = time.time()
S = my_sum_numba(10000000)
print(S)
print(time.time() - t1)

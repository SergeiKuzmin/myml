def print_dict(my_dict, final_str=''):
    for key in my_dict:
        str_tmp = final_str
        str_tmp += key
        if type(my_dict[key]) != dict:
            str_tmp += '.'
            str_tmp += str(my_dict[key])
            print(str_tmp)
        else:
            print_dict(my_dict[key], final_str=str_tmp + '.')


test_dict = {'a': {'b': {'c': 5, 'd': 10}}, 'e': 6}
print_dict(test_dict)

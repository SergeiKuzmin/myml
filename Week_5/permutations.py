def permute(nums):
    permutations = []
    if len(nums) == 1:
        permutations = [nums]
    else:
        for i in range(len(nums)):
            collect = permute(nums[:i] + nums[i + 1:])
            for per in collect:
                permutations.append([nums[i]] + per)
    return permutations


a = [1, 2, 3, 4, 5]
print(len(permute(a)))

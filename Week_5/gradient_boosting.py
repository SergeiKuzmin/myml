import pandas
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import log_loss

data = pandas.read_csv('gbm-data.csv')
data_numpy = data.values
data_X = data_numpy.T[1:].T
data_Y = data_numpy.T[0].T
print(data)
print(data_X)
print(data_Y)

X_train, X_test, Y_train, Y_test = train_test_split(data_X, data_Y, test_size=0.8, random_state=241)
list_learning_rates = [1, 0.5, 0.3, 0.2, 0.1]

log_loss_list_1_train = []
log_loss_list_1_test = []
log_loss_list_05_train = []
log_loss_list_05_test = []
log_loss_list_03_train = []
log_loss_list_03_test = []
log_loss_list_02_train = []
log_loss_list_02_test = []
log_loss_list_01_train = []
log_loss_list_01_test = []

clf = GradientBoostingClassifier(learning_rate=list_learning_rates[0], n_estimators=250, verbose=True,
                                 random_state=241)
clf.fit(X_train, Y_train)

for stage, y_pred in enumerate(clf.staged_decision_function(X_train)):
    log_loss_list_1_train.append(log_loss(Y_train, 1 / (1 + np.exp(-y_pred))))
for stage, y_pred in enumerate(clf.staged_decision_function(X_test)):
    log_loss_list_1_test.append(log_loss(Y_test, 1 / (1 + np.exp(-y_pred))))

clf = GradientBoostingClassifier(learning_rate=list_learning_rates[1], n_estimators=250, verbose=True,
                                 random_state=241)
clf.fit(X_train, Y_train)

for stage, y_pred in enumerate(clf.staged_decision_function(X_train)):
    log_loss_list_05_train.append(log_loss(Y_train, 1 / (1 + np.exp(-y_pred))))
for stage, y_pred in enumerate(clf.staged_decision_function(X_test)):
    log_loss_list_05_test.append(log_loss(Y_test, 1 / (1 + np.exp(-y_pred))))

clf = GradientBoostingClassifier(learning_rate=list_learning_rates[2], n_estimators=250, verbose=True,
                                 random_state=241)
clf.fit(X_train, Y_train)

for stage, y_pred in enumerate(clf.staged_decision_function(X_train)):
    log_loss_list_03_train.append(log_loss(Y_train, 1 / (1 + np.exp(-y_pred))))
for stage, y_pred in enumerate(clf.staged_decision_function(X_test)):
    log_loss_list_03_test.append(log_loss(Y_test, 1 / (1 + np.exp(-y_pred))))

clf = GradientBoostingClassifier(learning_rate=list_learning_rates[3], n_estimators=250, verbose=True,
                                 random_state=241)
clf.fit(X_train, Y_train)

for stage, y_pred in enumerate(clf.staged_decision_function(X_train)):
    log_loss_list_02_train.append(log_loss(Y_train, 1 / (1 + np.exp(-y_pred))))
for stage, y_pred in enumerate(clf.staged_decision_function(X_test)):
    log_loss_list_02_test.append(log_loss(Y_test, 1 / (1 + np.exp(-y_pred))))

clf = GradientBoostingClassifier(learning_rate=list_learning_rates[4], n_estimators=250, verbose=True,
                                 random_state=241)
clf.fit(X_train, Y_train)

for stage, y_pred in enumerate(clf.staged_decision_function(X_train)):
    log_loss_list_01_train.append(log_loss(Y_train, 1 / (1 + np.exp(-y_pred))))
for stage, y_pred in enumerate(clf.staged_decision_function(X_test)):
    log_loss_list_01_test.append(log_loss(Y_test, 1 / (1 + np.exp(-y_pred))))

fig, ax = plt.subplots()
plt.plot(log_loss_list_1_train, lw=1, alpha=1, label=r'$learning rate = 1.0, train$')
plt.plot(log_loss_list_1_test, '--', lw=1, alpha=1, label=r'$learning rate = 1.0, test$')
plt.plot(log_loss_list_05_train, lw=1, alpha=1, label=r'$learning rate = 0.5, train$')
plt.plot(log_loss_list_05_test, '--', lw=1, alpha=1, label=r'$learning rate = 0.5, test$')
plt.plot(log_loss_list_03_train, lw=1, alpha=1, label=r'$learning rate = 0.3, train$')
plt.plot(log_loss_list_03_test, '--', lw=1, alpha=1, label=r'$learning rate = 0.3, test$')
plt.plot(log_loss_list_02_train, lw=1, alpha=1, label=r'$learning rate = 0.2, train$')
plt.plot(log_loss_list_02_test, '--', lw=1, alpha=1, label=r'$learning rate = 0.2, test$')
plt.plot(log_loss_list_01_train, lw=1, alpha=1, label=r'$learning rate = 0.1, train$')
plt.plot(log_loss_list_01_test, '--', lw=1, alpha=1, label=r'$learning rate = 0.1, test$')
plt.legend(loc='lower right')
ax.minorticks_off()
# plt.xlim(0, 6)
# plt.ylim(0, 1.0)
plt.xlabel('Number of stages', fontsize=15)
plt.ylabel('Log loss', fontsize=15)
plt.show()

# print(log_loss_list_02_test, np.array(log_loss_list_02_test).min(), np.array(log_loss_list_02_test).shape)
for i in range(len(log_loss_list_02_test)):
    print(i + 1, log_loss_list_02_test[i])

clf = RandomForestClassifier(n_estimators=37, random_state=241)
clf.fit(X_train, Y_train)
Y_pred = clf.predict_proba(X_test)
print(log_loss(Y_test, Y_pred))

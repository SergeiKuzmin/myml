import pandas
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score, make_scorer
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score

data = pandas.read_csv('abalone.csv')

# print(data)

data['Sex'] = data['Sex'].map(lambda x: 1 if x == 'M' else (-1 if x == 'F' else 0))

print(data)

x_train = np.array(data.T[:-1].T)
y_train = np.array(data['Rings'])

print(x_train)
print(y_train)

n_splits = 5
gen_split = KFold(n_splits=n_splits, shuffle=True, random_state=1)

score_list = []
for n_estimators in range(1, 51, 1):
    clf = RandomForestRegressor(n_estimators=n_estimators, random_state=1)
    score = cross_val_score(clf, x_train, y_train, scoring=make_scorer(r2_score), cv=gen_split).mean()
    score_list.append((n_estimators, score))
    print('n_estimators = ', n_estimators, ' ', score)

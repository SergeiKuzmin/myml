import numpy as np
import pandas
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse
from sklearn.linear_model import Ridge

data_train = pandas.read_csv('salary-train.csv')
data_test = pandas.read_csv('salary-test-mini.csv')

# Training
data_train['FullDescription'] = pandas.DataFrame(map(lambda word: word.lower(), data_train['FullDescription']))
data_train['LocationNormalized'] = pandas.DataFrame(map(lambda word: word.lower(), data_train['LocationNormalized']))

data_train['FullDescription'] = data_train['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True)

data_train['LocationNormalized'].fillna('nan', inplace=True)
data_train['ContractTime'].fillna('nan', inplace=True)

vectorizer = TfidfVectorizer(min_df=5)
full_description_train = vectorizer.fit_transform(data_train['FullDescription'])

enc = DictVectorizer()
x_train_categ = enc.fit_transform(data_train[['LocationNormalized', 'ContractTime']].to_dict('records'))
x_train = sparse.hstack([full_description_train, x_train_categ])
y_train = data_train['SalaryNormalized']
print(x_train.shape)
clf = Ridge(alpha=1, random_state=241)
clf.fit(x_train, y_train)

# Test
data_test['FullDescription'] = pandas.DataFrame(map(lambda word: word.lower(), data_test['FullDescription']))
data_test['LocationNormalized'] = pandas.DataFrame(map(lambda word: word.lower(), data_test['LocationNormalized']))

data_test['FullDescription'] = data_test['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True)

data_test['LocationNormalized'].fillna('nan', inplace=True)
data_test['ContractTime'].fillna('nan', inplace=True)

full_description_test = vectorizer.transform(data_test['FullDescription'])
x_test_categ = enc.transform(data_test[['LocationNormalized', 'ContractTime']].to_dict('records'))
x_test = sparse.hstack([full_description_test, x_test_categ])
print(x_test.shape)
print(clf.predict(x_test))

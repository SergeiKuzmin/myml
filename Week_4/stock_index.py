import numpy as np
import pandas
from sklearn.decomposition import PCA

data = pandas.read_csv('close_prices.csv')
x_train = np.array((data.T[1:]).T)
print(x_train.shape)
pca = PCA(n_components=10)
pca.fit(x_train)
print(pca.explained_variance_ratio_)
print(np.array(pca.explained_variance_ratio_).cumsum())
x_train = pca.transform(x_train)
print(x_train.shape)
data_dj = pandas.read_csv('djia_index.csv')
data_dj = np.array((data_dj.T[1:]).T).reshape(-1)
print(data_dj.shape)
print(x_train.T[0].shape)
data_pearson = np.array([x_train.T[0], data_dj], dtype=float)
print(np.corrcoef(data_pearson))
print(pca.components_[0])
print(data.T[0])

import numpy as np


def psnr(float_r, float_g, float_b, float_r_target, float_g_target, float_b_target):
    mse = (1 / (3 * float_r.shape[0] * float_r.shape[1])) * (np.sum((float_r - float_r_target) ** 2) +
                                                             np.sum((float_g - float_g_target) ** 2) +
                                                             np.sum((float_b - float_b_target) ** 2))
    return -10 * np.log10(mse)

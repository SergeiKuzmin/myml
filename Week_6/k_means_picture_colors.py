import numpy as np
from skimage.io import imread
from skimage import img_as_float
from sklearn.cluster import KMeans
import copy
import matplotlib.pyplot as plt
import pylab
from Week_6.psnr import psnr

image = imread('parrots_4.jpg')

# print(image)
print(image.shape)

image = img_as_float(image)
# print(image)
# print(image.shape)
# fig, ax = plt.subplots()
# ax.imshow(image)
# plt.show()
# pylab.imshow(image)
# pylab.show()

shape_0 = np.shape(image)[0]
shape_1 = np.shape(image)[1]
shape_2 = np.shape(image)[2]

obj_features = np.reshape(image, (shape_0 * shape_1, shape_2))
print(obj_features.shape)

n_clusters = 11
clr = KMeans(n_clusters=n_clusters, init='k-means++', random_state=241)
clr.fit(obj_features)
print(clr.labels_[100], clr.labels_[1000], clr.labels_[3000])

list_of_clusters = []
for index_cluster in range(n_clusters):
    list_index_cluster = []
    for index_pixel, cluster in enumerate(clr.labels_):
        if cluster == index_cluster:
            list_index_cluster.append(index_pixel)
    list_of_clusters.append(np.array(list_index_cluster))
print(list_of_clusters)

obj_features_mean = copy.copy(obj_features)
obj_features_median = copy.copy(obj_features)

for index_cluster in range(n_clusters):
    tmp_np_array = []
    for rows_index in list_of_clusters[index_cluster]:
        tmp_np_array.append(obj_features[rows_index])
    # mean = np.mean(np.array(tmp_np_array))
    # median = np.median(np.array(tmp_np_array))
    mean_r = np.mean(np.array(tmp_np_array).T[0])
    mean_g = np.mean(np.array(tmp_np_array).T[1])
    mean_b = np.mean(np.array(tmp_np_array).T[2])
    median_r = np.median(np.array(tmp_np_array).T[0])
    median_g = np.median(np.array(tmp_np_array).T[1])
    median_b = np.median(np.array(tmp_np_array).T[2])
    for rows_index in list_of_clusters[index_cluster]:
        obj_features_mean[rows_index][0] = mean_r
        obj_features_mean[rows_index][1] = mean_g
        obj_features_mean[rows_index][2] = mean_b
        obj_features_median[rows_index][0] = median_r
        obj_features_median[rows_index][1] = median_g
        obj_features_median[rows_index][2] = median_b

image_mean = np.reshape(obj_features_mean, (shape_0, shape_1, shape_2))
image_median = np.reshape(obj_features_median, (shape_0, shape_1, shape_2))
# print(image_mean)
# pylab.imshow(image_mean)
# pylab.show()

print(psnr(image_mean[:, :, 0], image_mean[:, :, 1], image_mean[:, :, 2],
           image[:, :, 0], image[:, :, 1], image[:, :, 2]))
print(psnr(image_median[:, :, 0], image_median[:, :, 1], image_median[:, :, 2],
           image[:, :, 0], image[:, :, 1], image[:, :, 2]))
